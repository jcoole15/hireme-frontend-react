import React from 'react';
import PropTypes from 'prop-types';
import { Button, Popover, OverlayTrigger } from 'react-bootstrap';

class IndustryPopover extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editName: this.props.name
        }
    }

    render() {
        return (
            <Popover id="popover" title="Change name">
                <input onChange={(evt) => this.setState({editName: evt.target.value})} type="text" defaultValue={this.props.name} />
            <Button bsStyle="success" onClick={() => this.props.handleClick(this.state.editName)}>Ok</Button>
            </Popover>
        )
    }
};

const Industry = ({ name, handleEditClick, handleDeleteClick }) => {
    return (
        <li style={{ border: "1px solid black" }}>
            <OverlayTrigger rootClose trigger="click" placement="right" overlay={<IndustryPopover name={name} handleClick={handleEditClick}/>}>
                <Button bsStyle="info">Edit</Button>
            </OverlayTrigger>
            <Button bsStyle="danger" onClick={handleDeleteClick}>Delete</Button>
            {name}
        </li >
    );
};

Industry.propTypes = {
    name: PropTypes.string.isRequired
}

export default Industry;