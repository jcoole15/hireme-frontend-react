import React from 'react';
import Industry from './industry';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { fetchIndustries } from '../redux/actions';
import { industriesEndpoint, AuthHeaderGenerate } from '../globals';

async function addIndustry(dispatch, NAME) {
    let newIndustry = { ID: 0, NAME };
    await fetch(industriesEndpoint, { method: 'Post', headers: { ...AuthHeaderGenerate(), 'content-type': 'application/json' }, body: JSON.stringify(newIndustry) });
    dispatch(fetchIndustries());
}

async function editIndustry(dispatch, ID, NAME) {
    let editIndustry = { ID, NAME };
    await fetch(industriesEndpoint + ID, { method: 'Put', headers: { ...AuthHeaderGenerate(), 'content-type': 'application/json' }, body: JSON.stringify(editIndustry) });
    dispatch(fetchIndustries());
}

async function deleteIndustry(dispatch, ID) {
    await fetch(industriesEndpoint + ID, { method: 'Delete', headers: {...AuthHeaderGenerate(), 'content-type': 'application/json'}});
    dispatch(fetchIndustries());
}

class IndustryList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            newName: '',
        }
    }

    render() {
        return (
            <div>
                <Button onClick={() => addIndustry(this.props.dispatch, this.state.newName)} bsStyle="success">Add</Button>
                <input onChange={(evt) => this.setState({ newName: evt.target.value })} />
                <ul style={{ listStyleType: 'none' }}>
                    {
                        Object.keys(this.props.industries).map((industry) =>
                            (<Industry key={industry} name={this.props.industries[industry].name} handleEditClick={(name) => editIndustry(this.props.dispatch, industry, name)} handleDeleteClick={() => deleteIndustry(this.props.dispatch, industry)} />))
                    }
                </ul>
            </div>
        );
    }
};

IndustryList.propTypes = {
    industries: PropTypes.shape(
        {
            id: PropTypes.shape({
                name: PropTypes.string.isRequired
            })
        }
    ).isRequired
};

const mapStateToProps = state => {
    return { industries: state.industries.items };
}

const VisibleIndustryList = connect(mapStateToProps)(IndustryList);

const IndustryRoutes = () => (
    <React.Fragment>
        <Route path="/industry" component={VisibleIndustryList} />
    </React.Fragment>);

export { IndustryRoutes };