import React from 'react'
import { connect } from 'react-redux';
import { MenuItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

const industryNavItems = ({ industries, eventKeyIndex }) => {
    return Object.keys(industries).map((industry) => (
        <LinkContainer key={industry} to={'/search?search.industryID=' + industry}>
            <MenuItem eventKey={eventKeyIndex + 0.01 * (industry + 1)}>
                {industries[industry].name}
            </MenuItem>
        </LinkContainer>)
    )
}

const mapStateToProps = (state, ownProps) => {
    return {
        industries: state.industries.items,
        eventKeyIndex: ownProps.eventKeyIndex
    };
}

export default connect(mapStateToProps)(industryNavItems);