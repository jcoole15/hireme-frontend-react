import { REQUEST_INDUSTRIES, RECEIVE_INDUSTRIES, LOGIN_POST, LOGIN_SUCCESS, LOGIN_FAILURE, SET_ROLE, LOGOUT } from './actions';


const industries = (state = { isFetching: false, items: null }, action) => {
    switch (action.type) {
        case REQUEST_INDUSTRIES:
            return Object.assign({}, state, { isFetching: true });
        case RECEIVE_INDUSTRIES:
            let industries = Object.assign(...action.industries.map(elem => ({ [elem.ID]: { name: elem.Name } })));
            return Object.assign({}, state, { items: industries }, { isFetching: false });
        default:
            return state;
    }
}

const user = (state = { isLoggedIn: false, isLoggingIn: false, name: '', role: '', token: '' }, action) => {
    switch (action.type) {
        case LOGIN_POST:
            return Object.assign({}, state, { isLoggingIn: true });
        case LOGIN_SUCCESS:
            return Object.assign({}, state, { isLoggedIn: true, isLoggingIn: false, name: action.username, token: action.token });
        case LOGIN_FAILURE:
            return Object.assign({}, state, { isLoggingIn: false });
        case SET_ROLE:
            return Object.assign({}, state, { role: action.role });
        case LOGOUT:
            return Object.assign({}, { isLoggingIn: false, isLoggedIn: false, name: '', role: '', token: '' });
        default:
            return state;
    }
}

export { industries, user };