import * as reducers from './reducers';
import { combineReducers } from 'redux';

const  HireMeApp = combineReducers(reducers);

export default HireMeApp;