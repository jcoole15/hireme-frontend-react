import { TokenEndpoint, industriesEndpoint, accountEndpoint } from '../globals';
import { store, history } from '../App';

export const REQUEST_INDUSTRIES = 'REQUEST_INDUSTRIES';
export const RECEIVE_INDUSTRIES = 'RECEIVE_INDUSTRIES';
export const FETCH_INDUSTRIES = 'FETCH_INDUSTRIES';

function requestIndustries() {
    return {
        type: REQUEST_INDUSTRIES
    };
}

function receiveIndustries(json) {
    return {
        type: RECEIVE_INDUSTRIES,
        industries: json
    }
}

export function fetchIndustries() {
    return function (dispatch) {
        dispatch(requestIndustries());
        return fetch(industriesEndpoint)
            .then(
                response => response.json(),
                error => {
                    console.log("api request failure", error);
                    dispatch(receiveIndustries(null));
                })
            .then(
                json => {
                    dispatch(receiveIndustries(json));
                })
    }
}

export const LOGIN = 'LOGIN';
export const LOGIN_POST = 'LOGIN_POST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const SET_ROLE = 'SET_ROLE';
export const LOGOUT = 'LOGOUT';

function loginPost() {
    return {
        type: LOGIN_POST,
    }
}

export function loginSuccess(loginResponse) {
    return {
        type: LOGIN_SUCCESS,
        username: loginResponse.userName,
        token: loginResponse.access_token,
    }
}

function loginFailure() {
    return {
        type: LOGIN_FAILURE,
    }
}

function login(email, pass) {
    return function (dispatch) {
        dispatch(loginPost());
        let formData = { grant_type: 'password', username: email, password: pass };
        let formBody = "";
        formBody = Object.keys(formData).map(elem => encodeURIComponent(elem) + "=" + encodeURIComponent(formData[elem])).join("&");

        return fetch(TokenEndpoint, { method: "Post", body: formBody })
            .then(response => response.json(),
                error => { console.log('api error', error) }
            )
            .then(json => {
                localStorage.setItem('token', json.access_token);
                localStorage.setItem('userName', json.userName);
                localStorage.setItem('expires', json['.expires']);
                dispatch(loginSuccess(json));
            })
            .catch(error => { console.log('Login failed: ', error); dispatch(loginFailure()) });
    }
}

export function setRole(role = '') {
    return {
        type: SET_ROLE,
        role
    }
}

function loadRole() {
    return function (dispatch) {
        return fetch(accountEndpoint + 'GetRoles', { headers: { "Authorization": 'bearer ' + store.getState().user.token } })
            .then(response => response.json(),
                error => console.log('GetRole error occured ', error)
            )
            .then(
                json => {
                    if (json.length === 0) {
                        localStorage.setItem('userRole', '');
                        dispatch(setRole(''));
                    } else {
                        localStorage.setItem('userRole', json[0]);
                        dispatch(setRole(json[0]));
                    }
                }
            )
    }
}

export function doLogin(dispatch, email, pass) {
    dispatch(login(email, pass)).then(() => dispatch(loadRole())).then(() => history.push("/"));
}

export function logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('userName');
    localStorage.removeItem('userRole');
    localStorage.removeItem('expires');
    return {
        type: LOGOUT
    }
}