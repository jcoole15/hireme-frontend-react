import React from 'react';
import { NavDropdown, NavItem} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import IndustryNavItems from './industries/industryNav';

const mainNav = ({ isLoggedIn }) => (
    <React.Fragment>
      <LinkContainer to="/search"><NavItem eventKey={1}>Latest Jobs</NavItem></LinkContainer>
      <NavDropdown id="industryDropdown" title="View by industry" eventKey={2}>
        <IndustryNavItems EventKeyIndex={0.2} />
      </NavDropdown>
      {isLoggedIn && <LinkContainer to="/agent"><NavItem eventKey={3}>My Searches</NavItem></LinkContainer>}
      <LinkContainer to="/contact"><NavItem eventKey={4}>Contact</NavItem></LinkContainer>
    </React.Fragment>
  );
  
  mainNav.propTypes = {
    isLoggedIn: PropTypes.bool.isRequired
  }
  
  function mapStateToProps(state) {
    return {
      isLoggedIn: state.user.isLoggedIn
    }
  }
  
export default connect(mapStateToProps)(mainNav);