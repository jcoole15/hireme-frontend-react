import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import './App.css';

import { Router, Route, Switch } from 'react-router-dom';
import { IndexLinkContainer } from 'react-router-bootstrap';
import createBrowserHistory from 'history/createBrowserHistory';

import MainNav from './nav';
import UserNav, { UserNavRoutes } from './accounts/userNav.js';
import { RoleNavRoutes } from './accounts/roleNav';

import SearchResults from './joblistings/searchResults';
import Searches from './joblistings/searchList';

import { IndustryRoutes } from './industries/industryList';

import HireMeApp from './redux/hireMeReducer';
import { fetchIndustries, loginSuccess, setRole } from './redux/actions';


const initialState = {
  user: {
    isLoggedIn: false,
    isLoggingIn: false,
    name: '',
    role: '',
    token: ''
  },

  industries: {
    isFetching: false,
    items: {
    }
  },
};

export const store = createStore(HireMeApp, initialState, applyMiddleware(thunkMiddleware));
export const history = createBrowserHistory();

const MainRoutes = () => (
  <React.Fragment>
    <Route exact path="/" />
    <Route path="/search" component={SearchResults} />
    <Route path="/agent" component={Searches}/>
    <Route path="/contact" />
  </React.Fragment>
);

const Routes = () => (
  <React.Fragment>
    <MainRoutes />
    <UserNavRoutes />
    <RoleNavRoutes />
    <IndustryRoutes />
  </React.Fragment>
);

class App extends React.Component {

  componentDidMount() {
    if(Date.parse(localStorage.getItem('expires')) > Date.now()) {
      store.dispatch(loginSuccess({userName: localStorage.getItem('userName'), access_token: localStorage.getItem('token')}))
      store.dispatch(setRole(localStorage.getItem('userRole')));
    }
    store.dispatch(fetchIndustries());
  }

  render() {
    return (
      <Provider store={store}>
        <Router history={history}>
          <div className="App">
            <Navbar className="navbar-dark bg-dark" fluid>
              <Navbar.Header>
                <Navbar.Brand>
                  <IndexLinkContainer exact to="/">
                    <span>HireMe</span>
                  </IndexLinkContainer>
                </Navbar.Brand>
                <Navbar.Toggle />
              </Navbar.Header>
              <Navbar.Collapse>
                <Nav>
                  <MainNav />
                </Nav>
                <Nav pullRight>
                  <UserNav eventKeyIndex={5} />
                </Nav>
              </Navbar.Collapse>
            </Navbar>
            <Switch>
              <Routes />
            </Switch>
            <footer className="footer">
              <div className="container">
                <p id="jcccFooterText">Copyright &copy; 2018 Johnson&nbsp;County&nbsp;Community&nbsp;College</p>
              </div>
            </footer>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
