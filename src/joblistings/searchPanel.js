import React from 'react';
import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

export default class SearchPanel extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            Title: '',

        }
    }

    handleChangeTitle = (e) => this.setState({ Title: e.target.value});

    render() {
        return (
            <form>
                <FormGroup controlId="formTitleText">
                    <ControlLabel>Job Title:</ControlLabel>
                    <FormControl type="text" value={this.state.Title} placeholder="Enter Job Ttile" onChange={this.handleChangeTitle} />
                </FormGroup>
            </form>
        );
    }
}