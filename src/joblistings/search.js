import React from 'react';
import { Button } from 'react-bootstrap';
import { searchEndpoint, AuthHeaderGenerate } from '../globals';
import PropTypes from 'prop-types';
import { history } from '../App';

function goSearch(id) {
    fetch(searchEndpoint + id + '/Go?redirectUrl=' + encodeURIComponent(window.location.host) + '/search' , { headers: {...AuthHeaderGenerate()} })
    .then((response) => response.json())
    .then((json) => history.push(json.split('/')[1]));
};

const Search = ({id, name}) => {
    return (
        <li style={{ border: "1px solid black" }}>
            <Button onClick={() => goSearch(id)}>Go</Button>
            <Button>Delete</Button>
            {name}
        </li>
    )
};

Search.propTypes= {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired
}

export default Search;