import React from 'react';
import { connect } from 'react-redux';
import { searchEndpoint, AuthHeaderGenerate } from '../globals';
import Search from './search';

class Searches extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searches: []
        }
    }

    componentDidMount() {
        fetch(searchEndpoint, { headers: { ...AuthHeaderGenerate() } })
            .then((response) => response.json())
            .then((json) => this.setState({ searches: json }));
    }

    render() {       
            return (
                <div>
                    <ul>
                        {this.props.isLoggedIn && this.state.searches.map(elem => (<Search key={elem.SearchID} id={elem.SearchID} name={elem.Name} />))}
                    </ul>
                </div>
            );
    }
}

const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.user.isLoggedIn
    }
}

export default connect(mapStateToProps)(Searches);