import React from 'react';
import { NavDropdown, MenuItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';

const roleNavRoutes = ({role}) => {
    switch (role) {
        case 'Employer':
            return (<React.Fragment>
                <Route path="/posting/create" />
                <Route path="/posting/view" />
            </React.Fragment>)
        case 'Admin':
            return (<React.Fragment>
                <Route path="/posting/pending" />
                <Route path="/account/employersPending" />
                <Route path="/industry" />
            </React.Fragment>)
        default:
            return null;
    }
}


const RoleNav = ({role, eventkeyindex}) => {
    switch (role) {
        case 'Employer':
            return (<NavDropdown id="EmployerActions" eventKey={eventkeyindex} title={role + " Toolbox"}>
                <LinkContainer to="/posting/create"><MenuItem eventKey={eventkeyindex + 0.01} >Create new job posting</MenuItem></LinkContainer>
                <LinkContainer to="/posting/view"><MenuItem eventKey={eventkeyindex + 0.02} to="/posting/view">View my postings</MenuItem></LinkContainer>
            </NavDropdown>);
        case 'Admin':
            return (<NavDropdown id="AdminActions" eventKey={eventkeyindex} title={role + " Toolbox"}>
                <LinkContainer to="/posting/pending"><MenuItem eventKey={eventkeyindex + 0.01}>View pending job postings</MenuItem></LinkContainer>
                <LinkContainer to="/account/employersPending"><MenuItem eventKey={eventkeyindex + 0.02}>View pending employer accounts</MenuItem></LinkContainer>
                <LinkContainer to="/industry"><MenuItem eventKey={eventkeyindex + 0.03}>Manage Industries</MenuItem></LinkContainer>
            </NavDropdown>);
        default:
            return null;
    }
}

function routesMapStateToProps(state) {
    return {
        role: state.user.role,
    }
}

function navMapStateToProps(state,ownProps) {
    return {
        role: state.user.role,
        eventkeyindex: ownProps.eventkeyindex,
    }
}

const RoleNavRoutes = connect(routesMapStateToProps)(roleNavRoutes);
export default connect(navMapStateToProps)(RoleNav);
export { RoleNavRoutes };