import React from 'react';
import { connect } from 'react-redux';
import { FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';
import { doLogin } from '../redux/actions';
import './userForm.css';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        }
    }
    render() {
        return (
            <form id="userForm">
                <FormGroup>
                    <ControlLabel>Username</ControlLabel>
                    <FormControl type="text" onChange={(evt) => this.setState({username: evt.target.value})} />
                </FormGroup>
                <FormGroup>
                    <ControlLabel>Password</ControlLabel>
                    <FormControl type="password" onChange={(evt) => this.setState({password:evt.target.value})} /><br />
                </FormGroup>
                <Button onClick={() => this.props.onLoginClick(this.state.username, this.state.password)}>Login</Button>
            </form>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onLoginClick: (username, pass) => {
            doLogin(dispatch, username, pass);
        }
    }
}

export default connect(null, mapDispatchToProps)(Login);
