import React from 'react';
import { FormGroup, FormControl, ControlLabel, Button, Checkbox } from 'react-bootstrap';
import { history } from '../App';
import ReCAPTCHA from 'react-google-recaptcha';
import { accountEndpoint } from '../globals';
import './userForm.css';

function register(data) {
    fetch(accountEndpoint + 'Register', { method: 'post', headers: { 'content-type': 'application/json' }, body: JSON.stringify(data) })
        .then((response) => { if (response.status > 299) { console.log(response) } else { history.push("/account/newRegistration") } });
}

export class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Email: '',
            Password: '',
            ConfirmPassword: '',
            RecaptchaString: '',
            ApplyAsEmployer: false
        }
    }

    render() {
        return (
            <form id="userForm">
                <FormGroup>
                    <ControlLabel>E-Mail</ControlLabel>
                    <FormControl type="text" onChange={(evt) => this.setState({ Email: evt.target.value })} />
                </FormGroup>
                <FormGroup>
                    <ControlLabel>Password</ControlLabel>
                    <FormControl type="password" onChange={(evt) => this.setState({ Password: evt.target.value })} />
                </FormGroup>
                <FormGroup>
                    <ControlLabel>Confirm Password</ControlLabel>
                    <FormControl type="password" onChange={(evt) => this.setState({ ConfirmPassword: evt.target.value })} />
                </FormGroup>
                <ReCAPTCHA sitekey="6LfEWUcUAAAAALFXW8yenW9gvW22_uGfw5lJI_Ei" onChange={(evt) => this.setState({ RecaptchaString: evt })} />
                <FormGroup>
                    <Checkbox onChange={(evt) => this.setState({ ApplyAsEmployer: evt.target.value })} >Apply as Employer</Checkbox>
                </FormGroup>
                <Button onClick={() => register(this.state)}>Register</Button>
            </form>
        );
    }
}