import React from 'react';
import RoleNav from './roleNav.js';
import { NavDropdown, MenuItem, Navbar } from 'react-bootstrap';
import { Route } from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Login from './login';
import { Register } from './register';
import { newRegistration } from './newRegistration';
import { logout } from '../redux/actions.js';
import { history } from '../App';

const UserNavRoutes = () => (<React.Fragment>
    <Route path="/register" component={Register} />
    <Route path="/account/newRegistration" component={newRegistration} />
    <Route path="/login" component={Login} />
</React.Fragment>);


const UserNav = ({ user, eventkeyindex, onLogoutClick }) => {
    if (user.isLoggedIn) {
        return (
            <React.Fragment>
                <RoleNav user={user} />
                <NavDropdown id="userMenu" title={user.name} eventkeyindex={eventkeyindex}>
                    <Navbar.Text>Role: {user.role || 'User'}</Navbar.Text>
                    <MenuItem onClick={onLogoutClick}>Logout</MenuItem>
                </NavDropdown>
            </React.Fragment>
        )
    }
    else {
        return (
            <React.Fragment>
                <LinkContainer to="/register"><MenuItem>Register</MenuItem></LinkContainer>
                <LinkContainer to="/login"><MenuItem>Login</MenuItem></LinkContainer>
            </React.Fragment>
        )
    }
}

UserNav.propTypes = {
    user: PropTypes.shape({
        name: PropTypes.string.isRequired,
        role: PropTypes.string.isRequired,
        isLoggedIn: PropTypes.bool.isRequired,
        isLoggingIn: PropTypes.bool.isRequired,
        token: PropTypes.string.isRequired
    }).isRequired
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user,
        eventkeyindex: ownProps.eventkeyindex
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onLogoutClick: () => {
            dispatch(logout());
            history.push("/login");
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserNav);
export { UserNavRoutes };