export const ApiRoot = 'https://hiremebackend20180212064418.azurewebsites.net/';

export const TokenEndpoint = ApiRoot + 'Token';

export const ApiURL = ApiRoot + 'api/';

export const industriesEndpoint = ApiURL + 'Industries/';

export const accountEndpoint = ApiURL + 'Account/';

export const jobListingsEndpoint = ApiURL + 'JobListings/';

export const searchEndpoint = ApiURL + 'Searches/';

export const AuthHeaderGenerate = () => ({'Authorization': 'bearer ' + localStorage.getItem('token')});